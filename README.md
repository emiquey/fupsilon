# Project Title

We sketch here a shallow embedding of F\_Upsilon into Coq.  
F\_Upsilon is a calculus aimed at being the target of typed   
continuation-and-environment-passing style translations.   
See the paper:  

**A calculus of expandable stores.  
Continuation-and-environment-passing style translations.**  
*Hugo Herbelin, Étienne Miquey. LICS 2020.*   

available at:  
https://hal.archives-ouvertes.fr/hal-02557823

### Prerequisites

Having Coq (>8.9) installed should be enough.

## Authors

* **Étienne Miquey**


